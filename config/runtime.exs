import Config

# config/runtime.exs is executed for all environments, including
# during releases. It is executed after compilation and before the
# system starts, so it is typically used to load production configuration
# and secrets from environment variables or elsewhere. Do not define
# any compile-time configuration in here, as it won't be applied.
# The block below contains prod specific runtime configuration.
if config_env() == :prod do
  database_url = "ecto://postgres:postgres@localhost/hello_dev"
    # System.get_env("DATABASE_URL") ||
    #   raise """
    #   environment variable DATABASE_URL is missing.
    #   For example: ecto://USER:PASS@HOST/DATABASE
    #   """

  config :hello, Hello.Repo,
    # ssl: true,
    # socket_options: [:inet6],
    url: database_url,
    pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

  # The secret key base is used to sign/encrypt cookies and other secrets.
  # A default value is used in config/dev.exs and config/test.exs but you
  # want to use a different value for prod and you most likely don't want
  # to check this value into version control, so we use an environment
  # variable instead.
  secret_key_base = "QG4TyZ7Y1e6eAvbONjB71EDtVhuTgGtgsUjMR+ezeFExNL73UL6VyohA9Un2K3Sb"
    # System.get_env("SECRET_KEY_BASE") ||
    #   raise """
    #   environment variable SECRET_KEY_BASE is missing.
    #   You can generate one by calling: mix phx.gen.secret
    #   """

    config :hello, HelloWeb.Endpoint,
    server: true,
    http: [port: 4000,transport_options: [socket_opts: [:inet6]]],
    url: [host: "localhost", port: 4000],
    debug_errors: true,
    code_reloader: true,
    check_origin: false,
    secret_key_base: secret_key_base


  # ## Using releases
  #
  # If you are doing OTP releases, you need to instruct Phoenix
  # to start each relevant endpoint:
  #
  #     config :hello, HelloWeb.Endpoint, server: true
  #
  # Then you can assemble a release by calling `mix release`.
  # See `mix help release` for more information.

  # ## Configuring the mailer
  #
  # In production you need to configure the mailer to use a different adapter.
  # Also, you may need to configure the Swoosh API client of your choice if you
  # are not using SMTP. Here is an example of the configuration:
  #
  #     config :hello, Hello.Mailer,
  #       adapter: Swoosh.Adapters.Mailgun,
  #       api_key: System.get_env("MAILGUN_API_KEY"),
  #       domain: System.get_env("MAILGUN_DOMAIN")
  #
  # For this example you need include a HTTP client required by Swoosh API client.
  # Swoosh supports Hackney and Finch out of the box:
  #
  #     config :swoosh, :api_client, Swoosh.ApiClient.Hackney
  #
  # See https://hexdocs.pm/swoosh/Swoosh.html#module-installation for details.
end
