defmodule HelloWeb.UserSocket do
  use Phoenix.Socket

# mix phx.gen.socket User
 channel "room:*", HelloWeb.RoomChannel

  @impl true
  def connect(_params, socket, _connect_info) do
    {:ok, socket}
  end

  @impl true
  def id(_socket), do: nil
end
